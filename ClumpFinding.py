#! /usr/bin/env python

def clump_finding(sequence, k, L, t):
    clumps = []
    for i in range(0, len(sequence) - L + 1):
        distinct_k_mers = {}
        for j in range(i, (i + L) - k + 1):
            k_mer = sequence[j:j+k]    
            if not k_mer in distinct_k_mers:
                distinct_k_mers[k_mer] = 1
            else:
                distinct_k_mers[k_mer] += 1
        [clumps.append(c) for c, v in distinct_k_mers.items() if v >= t and c not in clumps]
        del distinct_k_mers
    return clumps

def clump_finding1(sequence, k, L, t):
    distinct_k_mers = {}

    # Find all distinct k-mers in sequence and their locations
    for i in range(0, len(sequence) - k + 1):
        k_mer = sequence[i:i+k]
        if not k_mer in distinct_k_mers:
            distinct_k_mers[k_mer] = [i]
        else:
            distinct_k_mers[k_mer].append(i)
    
    # Filter out all those with occurrence less than t. They can't possibly 
    # form (L, t)-clumps is they appear less than t times in the WHOLE sequence.
    k_mers_with_t_occurrences = dict((k_mer, locations) for k_mer, locations in distinct_k_mers.items() if len(locations) >= t)

    # Now filter further by checking that t of those occurrences are within L of each other.
    return [k_mer for k_mer, location in k_mers_with_t_occurrences.items() if within_interval(location, L, t, k)]

def within_interval(p, L, t, s=0):
    for base_location in range(0, len(p) - t + 1):
        if p[base_location + t - 1] - p[base_location] <= L - s:
            return True
    return False


def time_test(sequence, k, L, t, n=1000):
    cf_timings = []
    cf1_timings = []

    for i in range(0, n):
        print "CF time", i
        t0 = time.time()
        clump_finding(genome, k, L, t)
        cf_timings.append(time.time() - t0)
        print "CF1 time", i

    for i in range(0, n):
        t0 = time.time()
        clump_finding1(genome, k, L, t)
        cf1_timings.append(time.time() - t0)

    sum_cf_timings = sum(cf_timings)
    sum_cf1_timings = sum(cf1_timings)

    print sum_cf_timings, cf_timings


if __name__ == "__main__":
    import sys
    import time

    if len(sys.argv) == 2:
        with open(sys.argv[1], 'r') as fp:
            # Get the genome from the file
            genome = fp.readline().upper().strip('\n')
            # Get the k, L, t values from file as a tuple of strings and convert each to an 
            # int.
            k, L, t =  map(int, fp.readline().strip('\n').split(" "))

            # print k, L, t

            result = clump_finding1(genome, k, L, t)
            for clump in result:
                print clump,
            print ""
            print "%d different %d-mers form (%d,%d)-clumps." % (len(result), k, L, t)
    else:
        print "Usage: clumpfinding.py [FILE]"
